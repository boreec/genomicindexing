#ifndef _DUPLICATE_SEQUENCES_FILTERER_HPP_
#define _DUPLICATE_SEQUENCES_FILTERER_HPP_

#include <algorithm>
#include <cassert>
#include <fstream>
#include <map>
#include <string>
#include <vector>
#include "FastaParser.hpp"
#include "Sequence.hpp"

class DuplicateSequencesFilterer{
private:
  /* string used in the unified id */
  const std::string UNIFIED_HEADER = "UNI";

  /* amount of '0' in the unified id */
  const unsigned int UNIFIED_SIZE = 6;

  std::vector<Sequence> _unique_sequences;
  std::map<std::string, std::string> _duplicate_ids;
  
public:
  /*
    Initialize a DuplicateSequenceFilterer object and its attributes from a vector
    of sequences.
   */
  DuplicateSequencesFilterer(std::vector<Sequence>&);

  /* Getters */
  const std::vector<Sequence>& get_unique_sequences() const {return this->_unique_sequences;};
  const std::map<std::string, std::string>& get_duplicate_ids() const {return this->_duplicate_ids;};

  void to_tsv(const std::string filename) const;
  void to_fasta(const std::string filename) const;
};
#endif
