#ifndef __FASTQ_PARSER_HPP__
#define __FASTQ_PARSER_HPP__
#include <fstream>
#include <iostream>
#include <vector>
#include "Sequence.hpp"
class FastqParser{
public:
  /*
   * Parse a .fastq file and store the reads in a given Sequence vector.
   * */
  static void parse(const std::string& filename, std::vector<Sequence>& reads);
};
#endif
