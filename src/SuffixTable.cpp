#include "SuffixTable.hpp"
#include <iostream>

SuffixTable::SuffixTable(){
}

void SuffixTable::compute_suffix_vector(const Sequence& s, std::vector<std::pair<std::string_view, unsigned short>>& result){
  const std::string_view sequence = s.get_sequence();

  result.reserve(sequence.size() + 1);
  
  for(unsigned short i = 0; i < sequence.size();i++){
    std::pair<std::string_view, unsigned short> p(sequence.substr(i, sequence.size()), i);
    result.push_back(p);
  }
  
  result.push_back(std::pair<std::string_view, unsigned short>("", sequence.size()));  
}

void SuffixTable::sort_suffix_vector(std::vector<std::pair<std::string_view, unsigned short>>& v){  
  std::sort(v.begin(), v.end(), suffix_comparison);
}

void SuffixTable::compute_suffix_vectors(const std::vector<Sequence>& vs, std::vector<std::vector<unsigned short>>& result){
  result.reserve(vs.size());
  for(unsigned s = 0; s < vs.size(); ++s){
    std::vector<std::pair<std::string_view, unsigned short>> suffixes;
    compute_suffix_vector(vs[s], suffixes);
    sort_suffix_vector(suffixes);

    std::vector<unsigned short> suffixes_idx;
    suffixes_idx.reserve(suffixes.size());
    for(unsigned t = 0; t < suffixes.size(); t++){
      suffixes_idx.push_back(suffixes[t].second);
    }
    result.push_back(suffixes_idx);
  }
}

void SuffixTable::serialize(const std::string filename, const std::vector<Sequence>& seqs){
  std::ofstream file(filename, std::ios::out | std::ios::binary);
  if(!file.is_open()){
    std::cerr << "Can not open file " << filename << std::endl;
    exit(EXIT_FAILURE);
  }

  // Write the amount of suffix arrays.
  unsigned suffix_arrays_amount = seqs.size();
  file.write((char *)&suffix_arrays_amount, sizeof(unsigned));

  // Write for each suffix arrays, its index and the original sequence.
  for(unsigned i = 0; i < suffix_arrays_amount; ++i){

    // Write suffix array index.
    std::vector<std::pair<std::string_view, unsigned short>> suffixes;
    compute_suffix_vector(seqs[i], suffixes);
    sort_suffix_vector(suffixes);
    
    unsigned short sa_size = suffixes.size();
    file.write((char*)&sa_size, sizeof(unsigned short));
    for(unsigned short j = 0; j < sa_size; ++j){
      file.write((char*)&suffixes[j].second, sizeof(unsigned short));
    }

    // Write original sequence corresponding to suffix array.
    for(unsigned short k = 0; k < sa_size; ++k){
      char nt = seqs[i].get_sequence()[k];
      file.write(&nt, sizeof(char));
    }
  }
  file.close();
}

void SuffixTable::unserialize
(const std::string& filename, std::vector<std::vector<unsigned short>>& sequence_suffix_arrays, std::vector<Sequence>& seqs){
  std::ifstream file(filename, std::ios::in | std::ios::binary);

  if(!file.is_open()){
    std::cerr << "Can not open file " << filename << std::endl;
    exit(EXIT_FAILURE);
  }
    
  // Read the amount of suffix arrays.
  unsigned suffix_arrays_amount;
  file.read((char*)&suffix_arrays_amount, sizeof(unsigned));
  sequence_suffix_arrays.reserve(suffix_arrays_amount);

  // Read for each suffix arrays its index and then the original sequence.
  for(unsigned j = 0; j < suffix_arrays_amount; ++j){
    // Read suffix array index.
    std::vector<unsigned short> index;
    unsigned short index_size;
    file.read((char*)&index_size, sizeof(unsigned short));
    index.reserve(index_size);
    for(unsigned short k = 0; k < index_size; ++k){
      unsigned short idx;
      file.read((char*)&idx, sizeof(unsigned short));
      index.push_back(idx);
    }
    sequence_suffix_arrays.push_back(index);
    std::string seq = "";
    for(unsigned short l = 0; l < index_size; ++l){
      char nt;
      file.read(&nt, sizeof(char));
      seq += nt;
    }
    seqs.push_back(Sequence("?", seq));
  }
  file.close();
}

bool SuffixTable::naive_search(const Sequence& s1, const std::vector<unsigned short>& s1_index, const Sequence& s2){
  const std::string_view reference_sequence = s1.get_sequence();
  const std::string_view read_sequence = s2.get_sequence();
  if(read_sequence.size() > reference_sequence.size()){
    return false;
  }
  if(read_sequence == "" || read_sequence == reference_sequence){
    return true;
  }
  
  for(unsigned i = 0; i < s1_index.size(); ++i){
    const unsigned suffix_length = reference_sequence.size() - s1_index[i];

    // The read can only appear in a suffix that have at least the same size.
    if(read_sequence.size() <= suffix_length){
      const std::string_view prefix = reference_sequence.substr(s1_index[i], read_sequence.size());
      if(prefix > read_sequence){
	return false;
      }
      if(prefix == read_sequence){
	return true;
      }
    }
  }
  return false;
}


bool SuffixTable::binary_search(const Sequence& s1, const std::vector<unsigned short>& s1_index, const Sequence& s2){
  const std::string_view reference_sequence = s1.get_sequence();
  const std::string_view read_sequence = s2.get_sequence();

  if(read_sequence.size() > reference_sequence.size()){
    return false;
  }
  if(read_sequence == "" || read_sequence == reference_sequence){
    return true;
  }

  unsigned short l_idx = 0;
  unsigned short r_idx = s1_index.size() - 1;

  while(l_idx <= r_idx){
    unsigned m = (l_idx + r_idx) / 2;
    unsigned suffix_length = reference_sequence.size() - s1_index[m];
    int comparison;
    if(read_sequence.size() > suffix_length){
      comparison = reference_sequence.substr(s1_index[m]).compare(read_sequence.substr(0, suffix_length));
    }else{
      comparison = reference_sequence.substr(s1_index[m], read_sequence.size()).compare(read_sequence);
    }
    if(comparison == 0){
      return true;
    }
    else if(comparison < 0){
      l_idx = m + 1;
    }else{
      r_idx = m - 1;
    }
  }
  return false;
}

