CC	:= g++
CXXFLAGS :=-std=c++17 -Wall -Werror -O1 -O2
SRC_DIR := src
TARGETS	:= duplicate_filterer index query test_suffix_table

all: $(TARGETS)

example: cleanall all
	@echo "removing duplicate sequences..."
	./duplicate_filterer data/seqs_example.fa && echo
	@echo "building suffix array index..."
	./index uniques_seqs.fa SUFFIX_TABLE && echo
	@echo "retrieving reads from index..."
	./query suffix_table data/reads_example.fastq example.tsv
	@echo "results found with program: "
	@cat example.tsv
	@echo "results found with grep: "
#	Trick to count when word is in a poor complexity area.
	@grep "TTTTT"  uniques_seqs.fa | wc -l
	@grep "CCCC" uniques_seqs.fa | wc -l
	@grep "TAG" uniques_seqs.fa | wc -l
	@grep "CTTC" uniques_seqs.fa | wc -l
	@grep "GGGGGGGGGAAAAAAACCCC" uniques_seqs.fa | wc -l

duplicate_filterer: $(SRC_DIR)/Sequence.o $(SRC_DIR)/FastaParser.o $(SRC_DIR)/DuplicateSequencesFilterer.o
	$(CC) $(CXXFLAGS) -o $@ $^
	chmod a+x $@

index: $(SRC_DIR)/Sequence.o $(SRC_DIR)/FastaParser.o $(SRC_DIR)/SuffixTable.o $(SRC_DIR)/index.o
	$(CC) $(CXXFLAGS) -o $@ $^
	chmod a+x $@

query:  $(SRC_DIR)/Sequence.o $(SRC_DIR)/FastqParser.o $(SRC_DIR)/SuffixTable.o $(SRC_DIR)/query.o
	$(CC) $(CXXFLAGS) -o $@ $^
	chmod a+x $@

test_suffix_table: $(SRC_DIR)/Sequence.o $(SRC_DIR)/SuffixTable.o $(SRC_DIR)/test_suffix_table.o
	$(CC) $(CXXFLAGS) -o $@ $^
	chmod a+x $@

$(SRC_DIR)/test_suffix_table.o: $(SRC_DIR)/test_suffix_table.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/DuplicateSequencesFilterer.o:$(SRC_DIR)/DuplicateSequencesFilterer.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/index.o:$(SRC_DIR)/index.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/query.o:$(SRC_DIR)/query.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/Sequence.o: $(SRC_DIR)/Sequence.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/FastaParser.o: $(SRC_DIR)/FastaParser.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/FastqParser.o: $(SRC_DIR)/FastqParser.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

$(SRC_DIR)/SuffixTable.o: $(SRC_DIR)/SuffixTable.cpp
	$(CC) $(CXXFLAGS) -c $^ -o $@

clean:
	rm -f $(SRC_DIR)/*.o
	rm -f uniques_seqs.fa
	rm -f suffix_table
	rm -f *.tsv
	rm -f test_serialization

cleanall: clean
	rm -f $(TARGETS)

