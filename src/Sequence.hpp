#ifndef _SEQUENCE_HPP_
#define _SEQUENCE_HPP_

#include <string>

class Sequence{
private:
  std::string _header;
  std::string _seq;

  /* separator char between the header informations */
  const std::string HEADER_SEPARATOR = "|";
public:
  /**
   * Parameter 1 : sequence id
   * Parameter 2 : sequence itself
   */
  Sequence(const std::string &i,const std::string &j): _header(i), _seq(j) {};

  // GETTERS and SETTERS
  const std::string& get_sequence() const {return _seq;};
  const std::string& get_header() const{return _header;};

  const std::string get_id() const ;
  const std::string get_country() const;
  void set_id(std::string);
  void set_country(std::string);

  friend std::ostream& operator<< (std::ostream& stream, const Sequence& sequence);
};

#endif
