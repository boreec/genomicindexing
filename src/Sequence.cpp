#include "Sequence.hpp"
#include <iostream>
#include <algorithm>

const std::string Sequence::get_id() const{
  std::string res = "";

  size_t pos_separator = _header.find(HEADER_SEPARATOR);

  if(pos_separator != std::string::npos){
    res = _header.substr(0, pos_separator);

    // remove whitespaces if any
    res.erase(std::remove_if(res.begin(), res.end(), isspace), res.end());
  }
  return res;
}

void Sequence::set_id(const std::string new_id){
  const size_t pos_separator = _header.find(HEADER_SEPARATOR);
  if(pos_separator != std::string::npos){
    _header = new_id + " " + _header.substr(pos_separator, _header.length());
  }
}

void Sequence::set_country(const std::string new_country){
  const size_t pos_separator = _header.find_last_of(HEADER_SEPARATOR);
  if(pos_separator != std::string::npos){
    _header = _header.substr(0, pos_separator + 1) + new_country;

  }
}

const std::string Sequence::get_country() const{
  std::string res = "";
  const size_t pos_separator = _header.find_last_of(HEADER_SEPARATOR);

  if(pos_separator != std::string::npos){
    res = _header.substr(pos_separator + 1, _header.length());
  }
  return res;
}

std::ostream& operator<<(std::ostream& stream, const Sequence& sequence){
  stream << '>' << sequence._header << '\n' << sequence._seq << '\n';
  return stream;
}
