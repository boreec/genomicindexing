# Genomic Indexing - Cyprien Borée

For more information : https://boreec.fr

To build all executables :
```bash
make
make example # compile, and run a small example and compare the results with the ones by grep
make cleanall # to clean executables
```

## Dependancies

The program dependancies are :
- **C++17** : You need a suitable [compiler](https://en.wikipedia.org/wiki/C%2B%2B17#Compiler_support) to handle it. I took this version for various reasons :
  - **std::string_view** : To prevent string copy for read-only strings. It's useful during for the suffix array (on **std::string** *substr* do a copy).
  - **constexpr lambda** : To handle automatically the type of some *functor*, for example during the sort on suffix arrays, the comparison function uses this syntax. 
  - **structured binding declaration** : TO make TSV export easier.

### Suffix array : indexing

In its naive version  **O(n²log(n))**, the suffix array building takes **90 secondes** (*User Time*) and at most **968Mo** of RAM during execution (*Maximum resident size*).
```
$ /usr/bin/time -v ./index uniques_seqs.fa SUFFIX_TABLE
	Command being timed: "./index uniques_seqs.fa SUFFIX_TABLE"
	User time (seconds): 90.66
	System time (seconds): 1.35
	Percent of CPU this job got: 99%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 1:32.27
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 968076
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 241831
	Voluntary context switches: 47
	Involuntary context switches: 401
	Swaps: 0
	File system inputs: 0
	File system outputs: 3333528
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
```

Index size after serializing it in a binary file :
```
$ stat -c %s suffix_table
1706736609
```

So around **1.7Go**.

### Table des suffixes - query

In the program **query**, we look only for **MEM** (Maximal Exact Matches).

Naive search in a table of 1000 reads takes **1243 seconds** :
```
Command being timed: "./query suffix_table data/SRR11573919_1K.fastq ns_suffix.tsv"
	User time (seconds): 1243.78
	System time (seconds): 0.75
	Percent of CPU this job got: 99%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 20:44.57
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 1917708
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 479196
	Voluntary context switches: 1
	Involuntary context switches: 6227
	Swaps: 0
	File system inputs: 0
	File system outputs: 0
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
```

The binary search takes only **28 seconds** :
```
occurences counted: done !
	Command being timed: "./query suffix_table data/SRR11573919_1K.fastq bs_suffix.tsv"
	User time (seconds): 28.91
	System time (seconds): 0.67
	Percent of CPU this job got: 99%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:29.58
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 1917684
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 479194
	Voluntary context switches: 1
	Involuntary context switches: 118
	Swaps: 0
	File system inputs: 0
	File system outputs: 0
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
```
