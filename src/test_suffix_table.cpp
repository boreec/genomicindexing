#include <cassert>
#include <iostream>
#include "Sequence.hpp"
#include "SuffixTable.hpp"

void test_compute_suffix_vector(){
  Sequence s1("random header", "banana");
  SuffixTable st;
  std::vector<std::pair<std::string_view, unsigned short>> suffixes;
  st.compute_suffix_vector(s1, suffixes);
  assert(suffixes.size() == 7);
  assert(suffixes[0].first == "banana");
  assert(suffixes[1].first == "anana");
  assert(suffixes[2].first == "nana");
  assert(suffixes[3].first == "ana");
  assert(suffixes[4].first == "na");
  assert(suffixes[5].first == "a");
  assert(suffixes[6].first == "");
  for(unsigned int i = 0; i < suffixes.size(); i++){
    assert(i == suffixes[i].second);
  }
}

void test_sort_suffix_vector(){
  Sequence s1("random header", "banana");
  SuffixTable st;
  std::vector<std::pair<std::string_view, unsigned short>> suffixes;
  st.compute_suffix_vector(s1, suffixes);
  unsigned before_sorting_size = suffixes.size();
  st.sort_suffix_vector(suffixes);
  assert(suffixes.size() == before_sorting_size);
  assert(suffixes[0].first == "");
  assert(suffixes[1].first == "a");
  assert(suffixes[2].first == "ana");
  assert(suffixes[3].first == "anana");
  assert(suffixes[4].first == "banana");
  assert(suffixes[5].first == "na");
  assert(suffixes[6].first == "nana");

  assert(suffixes[0].second == 6);
  assert(suffixes[1].second == 5);
  assert(suffixes[2].second == 3);
  assert(suffixes[3].second == 1);
  assert(suffixes[4].second == 0);
  assert(suffixes[5].second == 4);
  assert(suffixes[6].second == 2);
}

void test_compute_suffix_vectors(){
  Sequence s1("random header", "banana");
  Sequence s2("another header", "apple");
  Sequence s3("yet a header", "orange");
  Sequence s4("still a header", "strawberry");

  const std::vector<Sequence> vs = {s1, s2, s3, s4};
  SuffixTable st;
  std::vector<std::vector<unsigned short>> index;
  st.compute_suffix_vectors(vs, index);
  assert(index.size() == 4);
  assert(index[0].size() == 7);
  assert(index[1].size() == 6);
  assert(index[2].size() == 7);
  assert(index[3].size() == 11);

  for(unsigned i = 0; i < index.size(); ++i){
    assert(index[i][0] == index[i].size() - 1);
  }
}

void test_serialization(){
  Sequence s1("UNI0000001", "banana");
  Sequence s2("UNI0000002", "apple");
  Sequence s3("UNI0000003", "strawberry");
  std::vector<Sequence> vs = {s1, s2, s3};
  SuffixTable st;
  std::vector<std::vector<unsigned short>> before_serializing;
  st.compute_suffix_vectors(vs, before_serializing);

  st.serialize("test_serialization", vs);

  std::vector<std::vector<unsigned short>> suffix_arrays_index;
  std::vector<Sequence> sequences;
  st.unserialize("test_serialization", suffix_arrays_index, sequences);
  
  assert(suffix_arrays_index.size() == before_serializing.size());
  assert(sequences.size() == 3);

  // Verify that index are correctly retrieved.
  for(unsigned sa = 0; sa < suffix_arrays_index.size(); ++sa){
    for(unsigned sa_idx = 0; sa_idx < suffix_arrays_index.at(sa).size(); ++sa_idx){
      assert(suffix_arrays_index.at(sa)[sa_idx] == before_serializing[sa][sa_idx]);
    }
  }

  // Verify that original sequences are correctly retrieved.
  for(unsigned current_seq = 0; current_seq < vs.size(); ++current_seq){
    for(unsigned c = 0; c < vs[current_seq].get_sequence().size(); ++c){
      assert(vs[current_seq].get_sequence()[c] == sequences.at(current_seq).get_sequence()[c]);
    }
  }
}

void test_naive_search(){
  const std::string seq_str = "Estas Tonne is the greatest contemporary musician. His "
    " virtuosity remains unequalled. Yet, he appears calm and confident, as if "
    " he was a solemn tree that nothing can disturb.";
  const Sequence s1("UNI0000001", seq_str);
  const Sequence s2("UNI0000002", "Estas Tonne");
  const Sequence s3("UNI0000003", "he");
  const Sequence s4("UNI0000004", "unfound str");
  const Sequence s5("UNI0000005", "c");

  SuffixTable st;
  std::vector<std::pair<std::string_view, unsigned short>> seq_suffixes;
  st.compute_suffix_vector(s1, seq_suffixes);
  st.sort_suffix_vector(seq_suffixes);

  std::vector<unsigned short> s1_sa_index;
  for(unsigned i = 0; i < seq_suffixes.size(); ++i){
    s1_sa_index.push_back(seq_suffixes[i].second);
  }

  assert(s1_sa_index.size() == seq_str.size() + 1);

  bool res_s2 = st.naive_search(s1, s1_sa_index, s2);
  bool res_s3 = st.naive_search(s1, s1_sa_index, s3);
  bool res_s4 = st.naive_search(s1, s1_sa_index, s4);
  bool res_s5 = st.naive_search(s1, s1_sa_index, s5);
  
  assert(res_s2);
  assert(res_s3);
  assert(!res_s4);
  assert(res_s5);
}
void test_binary_search(){
  const std::string seq_str = "AGGTTCCTTGGGCCTT";
  const Sequence s1("UNI0000001", seq_str);
  const Sequence s2("UNI0000002", "TT");
  const Sequence s3("UNI0000003", "GG");
  const Sequence s4("UNI0000004", "unfound str");
  const Sequence s5("UNI0000005", seq_str);
  const Sequence s6("UNI0000006","AGGTTCC");
  const Sequence s7("UNI0000007", "GGGCCTT");
  const Sequence s8("UNI0000008", "");
  const Sequence s9("UNI0000009", "AAGGTTCCTTGGGCCTT");
  
  SuffixTable st;
  std::vector<std::pair<std::string_view, unsigned short>> seq_suffixes;
  st.compute_suffix_vector(s1, seq_suffixes);
  st.sort_suffix_vector(seq_suffixes);

  std::vector<unsigned short> s1_sa_index;
  for(unsigned i = 0; i < seq_suffixes.size(); ++i){
    s1_sa_index.push_back(seq_suffixes[i].second);
  }

  assert(s1_sa_index.size() == seq_str.size() + 1);

  bool res_s2 = st.binary_search(s1, s1_sa_index, s2);
  bool res_s3 = st.binary_search(s1, s1_sa_index, s3);
  bool res_s4 = st.binary_search(s1, s1_sa_index, s4);
  bool res_s5 = st.binary_search(s1, s1_sa_index, s5);
  bool res_s6 = st.binary_search(s1, s1_sa_index, s6);
  bool res_s7 = st.binary_search(s1, s1_sa_index, s7);
  bool res_s8 = st.binary_search(s1, s1_sa_index, s8);
  bool res_s9 = st.binary_search(s1, s1_sa_index, s9);
  
  assert(res_s2);
  assert(res_s3);
  assert(!res_s4);
  assert(res_s5);
  assert(res_s6);
  assert(res_s7);
  assert(res_s8);
  assert(!res_s9);
}

int main(int argc, char* argv[]){
  test_compute_suffix_vector();
  test_sort_suffix_vector();
  test_compute_suffix_vectors();
  test_serialization();
  test_naive_search();
  test_binary_search();
  std::cout << "OK!" << std::endl;
  return 0;
}
