#include <fstream>
#include <iostream>
#include "FastaParser.hpp"

void FastaParser::parse(const std::string& filename, std::vector<Sequence>& result){
  std::fstream fasta_file(filename, std::ios::in);
  
  if(!fasta_file.is_open()){
    std::cerr << "Error while attempting to open " << filename << std::endl;
    exit(EXIT_FAILURE);
  }

  std::string line;
  std::string header = "";
  std::string sequence = "";
  while(getline(fasta_file, line)){
    if(line.back() == '\n')
      line.pop_back();
    
    if(line.front() == SEQUENCE_ID_FIRST_CHAR){
      if(sequence != "" && header != ""){
	result.push_back(Sequence(header, sequence));
	header = "";
	sequence = "";
      }
      header = line.substr(1);
    }
    else if(header != ""){
      sequence += line;
    }
  }
  if(header != "" && sequence != "")
    result.push_back(Sequence(header, sequence));
}
