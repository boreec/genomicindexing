#include "index.hpp"

void print_usage(){
  std::cout << "./index file.fa <index-method>" << std::endl;
  std::cout << "<index-method> can be \"SUFFIX_TABLE\"" << std::endl;
}

int main(int argc, char* argv[]){
  if(argc < 3){
    print_usage();
    exit(EXIT_FAILURE);
  }
  std::vector<Sequence> seqs;
  FastaParser::parse(argv[1], seqs);
  if(std::string(argv[2]) == std::string("SUFFIX_TABLE")){
    SuffixTable st;
    st.serialize("suffix_table", seqs);
  }else{
    std::cerr << "unknown suffix method. " << std::endl;
    exit(EXIT_FAILURE);
  }
  return 0;
}
