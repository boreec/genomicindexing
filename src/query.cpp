#include "query.hpp"

void print_usage(){
  std::cout << "./query index reads.fastq result.tsv" << std::endl;
  std::cout << "index : the index used to retrieve information." << std::endl;
  std::cout << "reads.fastq : a fastq file containing reads to look for in the index." << std::endl;
  std::cout << "result.tsv : the name of a tsv file where result will be written." << std::endl;
}

void count_occurrences(const std::vector<Sequence>& ref_seqs, const std::vector<std::vector<unsigned short>>& ref_idx,const std::vector<Sequence>& reads, std::map<std::string, unsigned short>& result){
  SuffixTable st;
  for(unsigned r = 0; r < reads.size(); ++r){
    unsigned short read_occurrences = 0;
    for(unsigned short s = 0; s < ref_seqs.size(); ++s){
      //read_occurrences += st.naive_search(ref_seqs[s], ref_idx[s], reads[r]);
      read_occurrences += st.binary_search(ref_seqs[s], ref_idx[s], reads[r]) ? 1 : 0;
    }
    if(read_occurrences){
      result[reads[r].get_header()] = read_occurrences;
    }
    std::cout << "read " << r << "/" << reads.size() << std::endl;
  }
}

void to_tsv(const std::string filename, const std::map<std::string, unsigned short>& occurrences){
  std::ofstream tsv_file(filename);
  if(!tsv_file.is_open()){
    std::cerr << "Error while attempting to write to the file " << filename << std::endl;
    exit(EXIT_FAILURE);
  }

  for(auto const& [key, val] : occurrences){
    tsv_file << key << "\t" << val << "\n";
  }
  tsv_file.close();

}

int main(int argc, char* argv[]){
  if(argc < 4){
    print_usage();
    exit(EXIT_FAILURE);
  }
  const std::string index_file = argv[1];
  const std::string reads_file = argv[2];
  const std::string result_file = argv[3];

  std::vector<std::vector<unsigned short>> suffix_arrays_index;
  std::vector<Sequence> suffix_arrays_sequence;
  std::vector<Sequence> reads;
  SuffixTable st;

  st.unserialize(index_file, suffix_arrays_index, suffix_arrays_sequence);
  std::cout << "unserialize done!" << std::endl;
  FastqParser::parse(reads_file, reads);
  std::cout << "reads parsed done!" << std::endl;
  std::map<std::string, unsigned short> count_results;
  count_occurrences(suffix_arrays_sequence, suffix_arrays_index, reads, count_results);
  std::cout << "occurences counted: done !" << std::endl;
  to_tsv(result_file, count_results);
  return 0;
}
