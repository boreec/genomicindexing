#include "DuplicateSequencesFilterer.hpp"
#include <iostream>

DuplicateSequencesFilterer::DuplicateSequencesFilterer(std::vector<Sequence>& seqs){
  std::vector<bool> seen_idx(seqs.size(), false);
  //unsigned int uniques = 0; // total number of unique sequences
  //unsigned int duplicates = 0; // total number of duplicate sequences
  unsigned int non_uniques = 0; // total number of non-unique sequences.
  std::string duplicate_ids;
  std::string duplicate_countries;
  std::string unified_id;
  bool duplicate_found;
  for(unsigned int i = 0; i < seqs.size(); i++){
    // Check the sequences for the unseen indexes.
    if(!seen_idx[i]){
      Sequence unique_sequence = seqs[i];
      duplicate_found = false;
      //uniques++;
      duplicate_ids = seqs[i].get_id();
      duplicate_countries = seqs[i].get_country();
      for(unsigned int j = i + 1; j < seqs.size(); j++){
	if(!seen_idx[j] && seqs[i].get_sequence() == seqs[j].get_sequence()){
	  duplicate_found = true;
	  duplicate_ids += "," + seqs[j].get_id();
	  seen_idx[j] = true;
	  // add country to the list only if it's not present
	  if(duplicate_countries.find(seqs[j].get_country()) == std::string::npos)
	    duplicate_countries += "," + seqs[j].get_country();
	  //duplicates++;
	}
      }
      if(duplicate_found){
	// build unified id
	const std::string unified_zeros = std::string(UNIFIED_SIZE - std::to_string(non_uniques).length(),'0');
	unified_id = UNIFIED_HEADER + unified_zeros + std::to_string(non_uniques);
	non_uniques++;
	_duplicate_ids[unified_id] = duplicate_ids;
	unique_sequence.set_id(unified_id);
	if(unique_sequence.get_country() != duplicate_countries)
	  unique_sequence.set_country(duplicate_countries);
      }
      seen_idx[i] = true;
      _unique_sequences.push_back(unique_sequence);
    }
  }
}

void DuplicateSequencesFilterer::to_tsv(const std::string filename) const{
  std::ofstream file(filename);

  if(!file.is_open()){
    std::cerr << "error while attempting to write to the file " << filename << std::endl;
    exit(EXIT_FAILURE);
  }
  
  for(auto const& [key, val] : _duplicate_ids){
    std::string tmp = val;
    std::replace(tmp.begin(), tmp.end(), ',', '\t');
    file << key << "\t" << tmp << "\n";
  }
  file.close();
}

void DuplicateSequencesFilterer::to_fasta(const std::string filename) const{
  std::ofstream file(filename);
  if(!file.is_open()){
    std::cerr << "error while attempting to write to the file " << filename << std::endl;
    exit(EXIT_FAILURE);
  }

  for(auto const& s: _unique_sequences){
    file << s;
  }
  file.close();
}

int main(int argc, char* argv[]){
  if(argc >= 2){
    std::vector<Sequence> seqs;
    FastaParser::parse(argv[1], seqs);

    DuplicateSequencesFilterer d(seqs);
    std::cout << "total sequences :\t" << seqs.size() << std::endl;
    std::cout << "uniques sequences :\t" << d.get_unique_sequences().size() << std::endl;
    std::cout << "duplicated sequences :\t" << d.get_duplicate_ids().size() << std::endl;
    
    d.to_tsv("duplicate_ids.tsv");
    std::cout << "duplicate_ids.tsv written" << std::endl;
    d.to_fasta("uniques_seqs.fa");
    std::cout << "uniques_seqs.fa written" << std::endl;
  }else{
    std::cout << "a multi-sequences fasta file is expected in argument" << std::endl;
  }
}

