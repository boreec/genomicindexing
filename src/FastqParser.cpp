#include "FastqParser.hpp"

void FastqParser::parse(const std::string& filename, std::vector<Sequence>& seqs){
  std::fstream fastq_file;
  fastq_file.open(filename, std::ios::in);
  if(!fastq_file.is_open()){
    std::cerr << "Trouble opening file " << filename << std::endl;
    exit(EXIT_FAILURE);
  }
  std::string line_str;
  std::string seq_header = "", seq = "";

  while(getline(fastq_file, line_str)){
    if(line_str[0] == '@'){
      seq_header = line_str.substr(1, line_str.size());
      continue;
    }
    if(seq_header != ""){
      // remove newline if there is one.
      if(line_str.back() == '\n'){
	line_str.pop_back();
      }
      seq = line_str;

      seqs.push_back(Sequence(seq_header, seq));
      seq = "";
      seq_header = "";	
    }
  }
}
