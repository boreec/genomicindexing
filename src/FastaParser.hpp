#ifndef __FASTA_PARSER_HPP__
#define __FASTA_PARSER_HPP__

#include <string>
#include <vector>
#include "FastaParser.hpp"
#include "Sequence.hpp"


class FastaParser{
private:
  static const char SEQUENCE_ID_FIRST_CHAR = '>';
public:
  /**
   * Parse a fasta file and store the result in a given Sequence vector.
   */
  static void parse(const std::string& filename, std::vector<Sequence>& seqs);    
};
#endif
