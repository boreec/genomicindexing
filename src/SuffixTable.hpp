#ifndef __SUFFIX_TABLE_HPP__
#define __SUFFIX_TABLE_HPP__
#include <algorithm>
#include <cassert>
#include <fstream>
#include <string>
#include <vector>
#include "Sequence.hpp"
class SuffixTable{
private:
  /* Compare two suffixes (aka pairs of string/integer) and is used for sorting algorithm. */
  static constexpr auto suffix_comparison =
    [](std::pair<std::string_view, unsigned short>& a,std::pair<std::string_view, unsigned short>& b){
      return a.first.compare(b.first) < 0;
    };
public:
  SuffixTable();

  /*
   * Build a suffix array in O(n) time, where n is the number of symbols in the given sequence.
   */
  void compute_suffix_vector(const Sequence& s, std::vector<std::pair<std::string_view, unsigned short>>& result);

  /*
   * Sort a suffix array in O(nlog(n)) time, where n is the number of suffixes.
   */
  void sort_suffix_vector(std::vector<std::pair<std::string_view, unsigned short>>& v);

  void compute_suffix_vectors(const std::vector<Sequence> &vs, std::vector<std::vector<unsigned short>>& result);

  /*
   * Build a suffix array and serialize it to  a binary file from a vector of sequences.
   * */
  void serialize(const std::string filename, const std::vector<Sequence>& seqs);

  /*
   * Read a serialized binary file and try to retrieve suffix arrays and associated sequence from it.
   */
  void unserialize(const std::string& filename, std::vector<std::vector<unsigned short>>& sa_index, std::vector<Sequence>& seqs);

  /*
   * Return true if s2 is inside s1, false otherwise.
   * Search is in O(n), very naive, does not really take advantage of sorted index.
   */
  bool naive_search(const Sequence& s1, const std::vector<unsigned short>& s1_index, const Sequence& s2);

  /*
   * Return true if s2 is inside s1, false otherwise.
   * Search is in O(log(n)), takes advantage of sorted index.
   */
  bool binary_search(const Sequence& s1, const std::vector<unsigned short>& s1_index, const Sequence& s2);
};
#endif
